/**
 * Created by marco on 02.09.14.
 */

(function ($) {
    var $document = $(document);

    $document.on('ready update', function (e) {
        var $target = $(e.target);

        $target.find('[data-slider-for]').each(function () {
            var $slider = $(this);
            var $input = $($slider.data('slider-for'));
            var $value = $slider.find('.js-slider-value');

            var setValue = function (factor, setInput) {
                var min = parseFloat($input.attr('min') || '0');
                var max = parseFloat($input.attr('max') || '100');
                var step = parseFloat($input.attr('step') || '1');

                var value = min + factor * (max - min); // transform 0–1 to min–max
                value = Math.round(value / step) * step; // snap to step size

                $value.css({width: factor * 100 + '%'});

                if (setInput) {
                    $input.val(value);
                    $input.trigger('change');
                }
            };

            $input.on('input change blur click reset-value', function () {
                var min = parseFloat($input.attr('min') || '0');
                var max = parseFloat($input.attr('max') || '100');

                var factor = ($input.val() - min) / (max - min);
                setValue(factor, false);
            });

            $slider.on('mousedown touchstart', function (e) {

                var handler = function (e) {
                    e.preventDefault();
                    var pageX = e.pageX;

                    if (e.type.substr(0, 5) === 'touch') {
                        pageX = e.originalEvent.touches[0].pageX;
                    }

                    var offset = $slider.offset();
                    var size = $slider.width();
                    var factor = (pageX - offset.left) / size;
                    factor = Math.max(0, Math.min(1, factor)); // limit to 0 – 1
                    setValue(factor, true);
                };

                handler(e);

                $document.on('mousemove touchmove', handler);

                $document.one('mouseup touchend touchcancel', function (e) {
                    $document.off('mousemove touchmove', handler);
                });
            });
        });
    });

})(jQuery);